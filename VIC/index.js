import auchandrive_v2 from '../index.js';
import data from 'tools/settings';
import format from 'format/formatClass/tile/VIC';
import style from 'format/formatClass/tile/VIC/main.less';
import bfStyle from './bf.less';

class template extends auchandrive_v2 {
    constructor() {
        super();
        this.format = new format();
        this.formatElementById;
    }
    build(crawl) {

        super.build(crawl);

        // for (var index in this.settings.productsbtns) {
        //     if (this.settings.productsbtns[index][2] === true) {
        //         for (var i = 0; i < this.settings.productsbtns[index][3].length; i++) {
        //             var mandatoryQty = 0;
        //             if (crawl[this.settings.productsbtns[index][3][i]]) {
        //                 mandatoryQty++;
        //             }
        //         }
        //         if (mandatoryQty < 1) {
        //             return false;
        //         }
        //     }
        // }

        //container = this.format.breakPoints(container, 577, '.sto-image-container');

        let container = this.format.html;
        let position = this.format.settings.position;


        this.helpers.createFormat(
            container,
            this.tracker,
            this.settings.products,
            this.products
        );

        // var target = document.querySelectorAll('.products-grid>.product-item')[0];
        this.positionFormat(container);

        // var testDummy = document.createElement('div');
        // testDummy.className = "sto-dummy products_grid";
        // testDummy.style.display = "none";
        //
        this.format.imageAssign();
        this.format.colorAssign();

        // var trackerImpVIC = {
        //     "pl": window.__sto.utils.fn.cleanString(container.querySelector('.sto-product-container>*').innerText),
        //     "pi": container.querySelector('.sto-product-container>*').getAttribute('data-id')
        // };
        //
        // this.format.trackFormat("impVIC", trackerImpVIC);

        this.responsivePos(container);
    }

    positionFormat(container) {
      let target,
          widthGrid = document.querySelector('.products-grid').offsetWidth,
          widthTile = document.querySelectorAll('.products-grid>.product-item')[0].offsetWidth,
          nbTilePerLine = Math.floor(widthGrid / widthTile),
          tile = document.querySelectorAll('.products-grid>.product-item');

      switch (nbTilePerLine) {
        case 1:
          target = tile[10];
          break;
        case 2:
          target = tile[10];
          break;
        case 3:
          target = tile[9];
          break;
        case 4:
          target = tile[8];
          break;
        case 5:
          target = tile[10];
          break;
        default:
      }
      target.parentNode.insertBefore(container, target);
    }

    init(tracker, settings) {
        this.format.getSettings(settings);
        this.format.getTrackers(tracker);
        this.settings = settings;
        this.tracker = tracker;
        this.format.init(settings);

        if (this.settings.type === "abk") {
            let container = this.format.html;
            style.use();
            bfStyle.use();
            this.crawl();
        } else {
            let container = this.format.html;
            let position = this.format.settings.position;
            style.use();
            var target = document.querySelectorAll('.mosaic>.prd')[2];
            target.parentNode.insertBefore(container, target);
        }
    }
}


var _init_ = function() {

    __sto.load("VIC", function(tracker, settings) {
        let t = new template();
        t.init(tracker, settings);

        return {
            "type": t.format.type,
            "crea_id": t.settings.cid,
            "existing": function() {
                return document.querySelector(".sto-container-" + t.format.type + "-" + t.settings.cid) !== null;
            },
            "remover": function() {
                var element = document.querySelector(".sto-container-" + t.format.type + "-" + t.settings.cid);
                element.parentNode.removeChild(element);
            }
        }

    })
};

export default _init_();
