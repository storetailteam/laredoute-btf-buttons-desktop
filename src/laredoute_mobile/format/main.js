"use strict";

console.log('DEV01');

try {

  const ctxt_pos = 0,
    ctxt_title = 1,
    ctxt_modal = 2,
    ctxt_prodlist = 2,
    ctxt_bg = 4,
    ctxt_logo = 5;

  var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    style = require("./main.css"),
    container = $(html),
    creaid = settings.creaid,
    format = settings.format,
    custom = settings.custom,
    products = [],
    btfpos = settings.btfpos,
    container = $(html),
    helper_methods = sto.utils.retailerMethod,
    redirect_url = settings.redirect,
    btnSize = settings.btnSize,
    left_txt = settings.left_text,
    crawl = settings.crawl.replace("https://drive.intermarche.com/", ""),
    myurl = window.location.href.replace("https://drive.intermarche.com/", ""),
    mydrive = myurl.split("/")[0],
    sto_global_nbproducts = 0,
    sto_global_products = [],
    sto_global_modal,
    promise,
    pdf,
    pos;

  var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
  $('head').first().append(fontRoboto);

  module.exports = {
    init: _init_()
  }

  function _init_() {

    sto.load([format, creaid], function(tracker) {

      var removers = {};
      removers[format] = {
        "creaid": creaid,
        "func": function() {
          style.unuse();
          container.remove(); //verifier containerFormat j'ai fait un copier coller
        }
      };

      var int, isElementInViewport;
      isElementInViewport = function(el) {
        if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
          el = el[0];
        }
        var rect = el.getBoundingClientRect();

        return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          !(rect.top == rect.bottom || rect.left == rect.right) &&
          !(rect.height == 0 || rect.width == 0) &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
          rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
      }

      //IF DEVICE IS NOT MOBILE STOP FORMAT
      // if (window.matchMedia('(min-width: 786px)').matches) {
      //   tracker.error({
      //     "tl": "onlyOnMobile"
      //   });
      //   return removers;
      //   return false;
      // }


      if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length > 0) {
        tracker.error({
          "tl": "placementOcuppied"
        });
        return removers;
        return false;
      }



      try {

        style.use();


        // console.log('hello');

        // <editor-fold> INSERT CONTAINER *****************************************************
        var insertFormat = function() {

            var widthTile = $('#productList>li').width();
            var widthLine = $('.product-list').width();
            var nbTile;

            if (widthTile * 2 > widthLine) {
              nbTile = 1;
            } else if (widthTile * 3 > widthLine) {
              nbTile = 2;
            } else {
              nbTile = 3;
            }

            var sponso = $('.hl-beacon-universal').length;
            if (sponso >= 1) {
              sponso = 1;
            }

            if ($('.sto-format').length == 0) {
              $('#productList>li').eq( nbTile * (5 + sponso)).before(container);
              $('.sto-format:first-of-type').attr('data-pos', 6 + sponso);
            } else if ($('.sto-format').length == 1) {
              $('#productList>li').eq( nbTile * (15 + sponso)).before(container);
              $('.sto-format:nth-of-type(2)').attr('data-pos', 16 + sponso);
            } else if ($('.sto-format').length == 2) {
              $('#productList>li').eq( nbTile * (25 + sponso)).before(container);
              $('.sto-format:nth-of-type(3)').attr('data-pos', 26 + sponso);
            }
          }

          insertFormat();

        // </editor-fold> *********************************************************************


        // if ($('div[data-pos="13"]').length <= 0) {
        //   pos = '13';
        //   $('#productList>li[data-productid]').eq(12).before(container);
        //   $('.sto-' + placeholder + '-w-butterfly').attr('data-pos', '13');
        // } else {
        //   pos = '25';
        //   $('#productList>li[data-productid]').eq(24).before(container);
        //   $('.sto-' + placeholder + '-w-butterfly').attr('data-pos', '25');
        // }

        $('.sto-' + placeholder + '-w-butterfly').attr('data-format-type', settings.format);
        $('.sto-' + placeholder + '-w-butterfly').attr('data-crea-id', settings.creaid);


        // <editor-fold> LEFT *****************************************************************

        // TEXTE
        if (left_txt && left_txt != "") {
          $(container).find('.sto-' + placeholder + '-left').prepend($("<p class='sto-" + placeholder + "-left-txt'>" + left_txt + "</p>"));
        }

        // </editor-fold> *********************************************************************


        var prodsInpage = $('#productList>li[data-productid]').length;

        //IF THERE ARE NOT ENOUGH PRODUCTS FOR ONE OR TWO OR IF THERER ARE ALREADY ENOUGH STOP FORMAT
        if (prodsInpage >= 14 && prodsInpage <= 20 && $('.sto-' + format).length < 1) {
          tracker.display({
            "po": pos
          });
        } else if (prodsInpage > 20 && $('.sto-' + format).length < 2) {
          tracker.display({
            "po": pos
          });
        } else {
          tracker.error({
            "tl": "notEnoughProdsInPage"
          });
          return removers;
          return false;
        }



        //VIEW TRACKING
        int = window.setInterval(function() {
          if (isElementInViewport(container)) {
            tracker.view({
              "po": pos
            });
            window.clearInterval(int);
          }
        }, 200);

        //INITIAL SETTINGS
        $('.sto-' + placeholder + '-w-vignette').attr('data-ind', '0');
        $('.sto-' + placeholder + '-w-vignette').attr('data-href', settings.custom[0][3]);
        $('.sto-' + placeholder + '-content').append('<div class="sto-' + placeholder + '-w-buttons"></div>');
        if (settings.text_cta != "") $(".sto-" + placeholder + "-cta").append('<span>' + settings.text_cta + '</span>');
        if (settings.claim_text != "") $(".sto-" + placeholder + "-claim").append('<span>' + settings.claim_text + '</span>');


        //ADD BUTTONS
        for (var i = 0; i < settings.custom.length; i++) {
          $('.sto-' + placeholder + '-w-buttons').append('<button id="' + settings.custom[i][2][0] + '" data-index="' + (parseInt(settings.custom[i][0]) + 1) + '"><span>' + settings.custom[i][1] + '</span></button>');
        }

        //INITIAL DISPLAY
        $('.sto-' + placeholder + '-w-buttons').attr("size-product", sto_global_nbproducts);
        firstDisplay();

        //SELECT BUTTONS
        $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').on('click', function() {
          tracker.browse({
            "tl": $(this).attr('id'),
            "po": pos
          });
          selectProduct($(this).attr('data-index'));
        });

        //PRODUCT REDIRECTION
        $('.sto-' + placeholder + '-w-vignette').off().on('click', function() {
          tracker.moreInfo({
            'tl': 'productIndex' + $(this).attr('data-ind'),
            "po": pos
          });
          window.open($(this).attr('data-href'), '_self');
        });

        if ($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').length <= 1) {
          $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').hide();
        }


        //CENTER CONTENT
        var centerContent = function() {
          var logoH = $(".sto-" + placeholder + "-w-logo").height();
          var contentH = $(".sto-" + placeholder + "-content").height();
          var selectionH = $(".sto-" + placeholder + "-selection").height();
          if ($(".sto-" + placeholder + "-w-cta")) {
            var recetteH = $(".sto-" + placeholder + "-w-cta").height();
          } else {
            var recetteH = 0;
          }
          var contentPos = (selectionH - (contentH + recetteH + logoH)) / 2;
          $(".sto-" + placeholder + "-content").css('margin-top', contentPos + 'px');
        };

        $("body").on("click", ".sto-" + placeholder + "-cta", function() {
          tracker.click({
            "tl": "cta",
            "po": pos
          });
          window.open(settings.redirect, settings.target);
        });

        var btffHomote;
        window.addEventListener("resize", function() {

          var resizeFormat = function() {
              var widthTile = $('#productList>li').width();
              var widthLine = $('.product-list').width();
              var nbTile;

              if (widthTile * 2 > widthLine) {
                nbTile = 1;
              } else if (widthTile * 3 > widthLine) {
                nbTile = 2;
              } else {
                nbTile = 3;
              }

              $('.sto-format').each(function() {
                var dataPos = $(this).attr('data-pos');

                if (window.matchMedia("(max-width: 767px)").matches) {
                  $('#productList>li:visible').eq( nbTile * (dataPos - 1)).before(this);
                } else {
                  $('#productList>li').eq( nbTile * (dataPos - 1)).before(this);
                }

              });
            }
            resizeFormat();

          //BTF height homotetique
          $('.sto-' + placeholder + '-w-butterfly').css('height', ((77.3 * $('.sto-' + placeholder + '-w-butterfly').width()) / 100) + 'px');
          //Buttons height homotetique
          $('.sto-' + placeholder + '-cta,  .sto-' + placeholder + '-logo').css('height', ((25 * $('.sto-' + placeholder + '-logo').width()) / 100) + 'px');

          $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons>button').css('height', ((25 * $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons>button').width()) / 100) + 'px');

          centerContent();
        });
        window.setTimeout(function() {
          //window.dispatchEvent(new window.Event("resize"));
          var evt = window.document.createEvent('UIEvents');
          evt.initUIEvent('resize', true, false, window, 0);
          window.dispatchEvent(evt);
        });

        // STORE FORMAT FOR FILTERS
        // sto.globals["print_creatives"].push({
        //   html: $('.sto-' + placeholder + '-w-butterfly'),
        //   parent_container_id: "#productList",
        //   type: settings.format,
        //   crea_id: settings.creaid,
        //   position: pos
        // });

      } catch (e) {
        console.log(e);
        style.unuse();
        container.remove();
        tracker.error({
          "te": "onBuild-format",
          "tl": e,
          "po": pos
        });
      }
      //}).then(null, console.log.bind(console));
      return removers;
    });

  }

  function verifAvailability(a, d) {
    var l = a[ctxt_prodlist].length;
    for (var i = 0; i <= (l - 1); i++) {
      var value = a[ctxt_prodlist][i];
      if (d[value] !== undefined && d[value].dispo !== false) {
        sto_global_products.push(value);
        sto_global_nbproducts += 1;
        return value;
      }
    }
    return false;
  }

  function addButton(cug, title, index) {
    $('.sto-' + placeholder + '-w-buttons').append('<button id="' + cug + '" data-index="' + (index + 1) + '"><span>' + title + '</span></button>');
  }

  function browseObj(index, d, tracker) {
    var currentbObj = custom[index];
    if (currentbObj) {
      var title = currentbObj[ctxt_title],
        modal = currentbObj[ctxt_modal],
        available = verifAvailability(currentbObj, d);
      modal = modal === "true" ? true : false;

      if (available != false) {
        if (modal) {
          sto_global_modal = true;
        }
        addButton(available, title, index, tracker);
      }
    }
  }

  function firstDisplay() {
    $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette_produit_info:first-child').show();
    $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).addClass('sto-' + placeholder + '-selButton');
    if (settings.custom_images === true) {
      $('.sto-' + placeholder + '-w-butterfly').attr('data-index', $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).attr('data-index'));
    }
    $('.sto-' + placeholder + '-w-vignette').attr('data-href', settings.custom[0][2]);
  }

  function selectProduct(index) {
    $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').removeClass('sto-' + placeholder + '-selButton');
    $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button[data-index="' + index + '"]').addClass('sto-' + placeholder + '-selButton');
    index = parseInt(index) - 1;
    $('.sto-' + placeholder + '-w-vignette').attr('data-ind', index);
    $('.sto-' + placeholder + '-w-vignette').attr('data-href', settings.custom[index][2]);
  }

} catch (e) {
  console.log(e);
  console.log('ER_STO_TRYC');
}
