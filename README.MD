#HIGHCO

##TAG

javascript:void(jQuery.getScript('https://rscdn.storetail.net/ST-LEC1606/tag.js?'))

##SETTINGS


*retailer* =  Select retailer --string-- on peut choisir les diferent type de retailer 

*template* = : Select format-type --string--

*gutters_template* = Select Gutters --string--

*crawl* = Charge crawl-url --string / URL-- le highco ne besoin pas le crawl, il est vide pour defaut

*redirect* = Charge redirection-url --string / URL or void -- // si il n'y as pas de valeur la redirection est desactiver

*name* = Charge class-name html/css/js --string--

*products* = Charge id's --ARRAY-- le highco ne besoin pas les products, il est vide pour defaut

*format* = Select format-type-ERMS --string--

*live* = Select Environnement (Local/CDN) --Boolean--

*devPath* = Charge url local --string / URL--

*livePath* = Charge CDN local --string / URL--

*containeBgC* = background-color container --string / Hexadecimal or RGB--

*container_img* = image des container --string / IMG NAME.TYPE de FICHER--
